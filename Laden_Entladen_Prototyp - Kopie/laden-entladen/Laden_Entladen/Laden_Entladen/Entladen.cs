﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laden_Entladen
{
    public partial class Entladen : Form
    {
        Form1 Form1_Size;
        Object abl = new Object();
        MessungLaeuft messungLaeuft;

        // Neue Variablen anlegen für Werteübergabe aus Form1 und für mögliche weitere Verwendungen

        private int _Ladestrom;
        private int _Ladespannung;
        private int _Kapazität;
        private int _Pausen_Zeit;
        private int _Entladestrom;
        private int _Entladespannung;
        private int _Entlade_Zeit;
        private string _Zeit_ComboBox_Laden;
        private string _Zeit_ComboBox_Entladen;
        private bool save_finish;


        public Entladen(int Ladestrom, int Ladespannung, int Kapazität, int Pausen_Zeit, string Zeit)
        {
            InitializeComponent();
            this.comboBox1.Items.AddRange(new string[] { "h", "min", "sec" });


            // Variablen aus Form1 in neue Variablen in EntladeForm schreiben
            _Ladestrom = Ladestrom;
            _Ladespannung = Ladespannung;
            _Kapazität = Kapazität;
            _Pausen_Zeit = Pausen_Zeit;
            _Zeit_ComboBox_Laden = Zeit;
            
        }


        public void Set_Size_ent(Form1 _Form1_Size, Entladen _ent)                                //Methode für Weiter von Form1(Fenster anpassen)
        {
            _ent.DesktopBounds = _Form1_Size.DesktopBounds;
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void button2_Click(object sender, EventArgs e)                                     //Zurück zu Form1 (Laden)
        {
            Form1_Size = new Form1();
            Form1_Size.Set_Size_Form1(this, Form1_Size);
            Form1_Size.Show();

            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)                                       
        {
            
            SaveFileDialog sfd = new SaveFileDialog();                          // SaveFileDialaog erzeugen
           
            sfd.Filter = "txt - Daten(*.txt)|*.txt";                            //Dateityp festlegen
            sfd.InitialDirectory = @"/Users/frankbreitenfellner/";              // Start-Verzeichnis

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = new StreamWriter(sfd.OpenFile());                                         //Datei erstellen
                writer.WriteLine("Lade Daten");                                                                 // Überschrift
                writer.WriteLine($"Ladestrom = {_Ladestrom} A");                                                // EntladeStrom in Datei schreiben
                writer.WriteLine($"Ladespannung = {_Ladespannung} V");                                          // Ladespannung in Dateie schreiben
                writer.WriteLine($"Kapazität = {_Kapazität} mAh");                                                  // Kapazität in Datei schreiben
                writer.WriteLine($"Pause = {_Pausen_Zeit} {_Zeit_ComboBox_Laden}");                                   // Pausen_Zeit in Datei schreiben mit Combobox Inhalt
                writer.WriteLine(" ");                                                                          // Absatz in Datei
                writer.WriteLine("Entlade Daten");                                                              // Überschrift
                writer.WriteLine($"Entladestrom = {_Entladestrom = int.Parse(textBox1.Text)} A");                 // Entladestrom in Datei schreiben
                writer.WriteLine($"Entladespannung = {_Entladespannung = int.Parse(textBox2.Text)} V");           // Entladespannung in Datei schrieben
                writer.WriteLine($"Entlade Zeit = {_Entlade_Zeit = int.Parse(textBox3.Text)} {_Zeit_ComboBox_Entladen = comboBox1.Text}");     // Entlade_Zeit in Datei schreiben
                writer.Close();
                save_finish = true;

            }

            if (save_finish == true)
            {
                messungLaeuft = new MessungLaeuft();
                messungLaeuft.Show();
              
                this.Hide();
            }
            
        }

        public void textBox1_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            int EntladeSpannung = Convert.ToInt32(textBox2.Text);

        }

        public void textBox3_TextChanged(MessungLaeuft _Zeit, EventArgs e)
        {
            int EntladeZeit = Convert.ToInt32(textBox1.Text);
            string Zeit = comboBox1.Text;
            
            

            int delay = 0;
            if (Zeit == "h")
            {
                delay = 3600000;     //Umrechnung h in mSec

            }
            else if (Zeit == "min")
            {
                delay = 60000;       //Umrechnung min in mSec
            }
            else if (Zeit == "sec")
            {
                delay = 1000;
            }

            //System.Threading.Thread.Sleep(EntladeZeit * delay);

            abl = EntladeZeit * delay;

            //Halleluja

            
        }

        private void Entladen_Load(object sender, EventArgs e)
        {

        }
    }
               
        
        
    
}
