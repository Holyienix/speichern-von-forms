﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laden_Entladen
{
    public partial class MessungLaeuft : Form
    {

        
        MessungAbgeschlossen messungAbgeschlossen = new MessungAbgeschlossen();

     

        private void MessungLaeuft_Load(object sender, EventArgs e)
        {

        }

      



        public MessungLaeuft()
        {
            InitializeComponent();

            backgroundWorker1.WorkerReportsProgress = true;
            // backgroundWorker1.WorkerSupportsCancellation = true;
            // This event will be raised on the worker thread when the worker starts
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            // This event will be raised when we call ReportProgress
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            backgroundWorker1.RunWorkerAsync();
        }

        void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // The progress percentage is a property of e
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button1.Show();
        }


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            
            button1.Hide();
            // Ablauf Ladebalken
            for (int i = 0; i < 100; i++)
            {
                int percent = (int)(((double)progressBar1.Value / (double)progressBar1.Maximum) * 100);
                progressBar1.CreateGraphics().DrawString(percent.ToString() + "%", new Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(progressBar1.Width / 2 - 10, progressBar1.Height / 2 - 7));



                // Raport an Haupt Thread

                backgroundWorker1.ReportProgress(i);
               
                
                // Simuliere Programm
                System.Threading.Thread.Sleep(100);
                
            }
               
               
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            messungAbgeschlossen.Show();
            this.Close();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
