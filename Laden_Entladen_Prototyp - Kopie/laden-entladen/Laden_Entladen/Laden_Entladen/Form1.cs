﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laden_Entladen
{


    public partial class Form1 : Form
    {
        Entladen ent;
        static int Pausen_Zeit;
        static string Zeit;                    //Zeit aus Combobox auswahl

        public Form1()
        {
            InitializeComponent();
            this.comboBox1.Items.AddRange(new string[] { "h", "min", "sec" });
            
        }

        public void Set_Size_Form1(Entladen _ent, Form1 _Form1_Size)                  // Methode für zurück zu Form1 (Fenster anpassen)
        {
            _Form1_Size.DesktopBounds=_ent.DesktopBounds;
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)                         // Weiter zu Entladen
        {
            // Variablen festlegen zum überschreiben in EntladeForm ud dann zum abspeichern in der Datei

            int Ladestrom = Convert.ToInt32(textBox1.Text);
            int Ladespannung = Convert.ToInt32(textBox2.Text);
            int Kapazität = Convert.ToInt32(textBox3.Text);
            int Pausen_Zeit = int.Parse(textBox4.Text);
            Zeit = comboBox1.Text;
            
            ent = new Entladen(Ladestrom,Ladespannung,Kapazität,Pausen_Zeit,Zeit);           // Übergabe der Variabelen In EntladeForm
            ent.Set_Size_ent(this, ent);       
            ent.Show();


            this.Hide();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
          
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            int Pause = Convert.ToInt32(textBox1.Text);
            Zeit = comboBox1.Text;

            int delay = 0;
            if (Zeit == "h")
            {
                delay = 3600000;     //Umrechnung h in mSec

            }
            else if(Zeit == "min")
            {
                delay = 60000;       //Umrechnung min in mSec
            }
            else if (Zeit == "sec")
            {
                delay = 1000;
            }

            Pausen_Zeit = Pause * delay;

            System.Threading.Thread.Sleep(Pause * delay);
        }

    }
}
